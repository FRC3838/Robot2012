package frc3838.robot.utils;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.NoSuchElementException;
import java.util.Vector;

/**
 * A class used to indicate the level of logging messages that are logged to the console. Levels are accessed as public static constants. For example, to the
 * logging level of {@code INFO} is accessed as {@code LogLevel.INFO} Possible values, and their suggested usage, are as follows.  Levels are listed from the
 * least verbose, to the most verbose. <ol> <li>{@link #OFF}: no logging occurs.</li> <li>{@link #ERROR}: Only error messages are logged</li> <li>{@link #INFO}:
 * Only error and information messages are logged</li> <li>{@link #DEBUG}: A more verbose level in which debugging information, along with error and information
 * messages are logged</li> <li>{@link #TRACE}: The most verbose level in which all messages are logged including the highly verbose trace level messages</li>
 * </ol> Please see the {@link LogLevel} class documentation, and the individual logging methods of this class, for guidance on what types of messages are
 * appropriate for each level. <p /> <small> Note that this class is a Type Safe Enumeration Constants class, compatible with Java 1.4 (and earlier). It is
 * based on the Java World article <a href="http://www.javaworld.com/jw-07-1997/jw-07-enumerated.html"> Create enumerated constants in Java</a>. </small>
 */
public class LogLevel
{
    private String name;
    public final int ordinal;
    private LogLevel previous;
    private LogLevel next;
    private static LogLevel first = null;
    private static LogLevel last = null;
    private static int upperBound = 0;
    private static Hashtable values = new Hashtable();
    /** Turns logging OFF. No logging statements will be sent to the console. */
    public static final LogLevel OFF = new LogLevel("OFF");
    /** Logs only ERROR level messages to the console. INFO, DEBUG, and TRACE level messages will not be logged. */
    public static final LogLevel ERROR = new LogLevel("ERROR");
    /** Logs INFO and ERROR messages to the console. DEBUG and TRACE level messages will not be logged. */
    public static final LogLevel INFO = new LogLevel("INFO");
    /** Logs DEBUG, INFO and ERROR messages to the console. TRACE level messages will not be logged. */
    public static final LogLevel DEBUG = new LogLevel("DEBUG");
    /** Logs ALL levels of messages (ERROR, INFO, DEBUG & TRACE) to the console. */
    public static final LogLevel TRACE = new LogLevel("TRACE");

    //Lazy initialized collection objects
    private static Vector vector;
    private static LogLevel[] array;
    private static String[] nameArray;


    /**
     * Constructs a new LogLevel enumeration instance (i.e. constant). This constructor is private so that outside class cannot add any enumeration instances.
     *
     * @param name the name of the enumeration instance (i.e. constant), typically in all uppercase per standard naming conventions
     */
    private LogLevel(String name)
    {
        this.name = name;
        ordinal = upperBound++;
        if (first == null) first = this;
        if (last != null)
        {
            previous = last;
            last.next = this;
        }
        last = this;
        values.put(name, this);
    }
    /**
     * Returns an {@link java.util.Enumeration} of all <tt>LogLevel</tt>s.
     *
     * @return an Enumeration of all <tt>LogLevel</tt>s
     */
    public static Enumeration getEnumeration()
    {
        return new Enumeration()
        {
            private LogLevel current = first;
            public boolean hasMoreElements()
            {
                return current != null;
            }
            public Object nextElement()
            {
                if (current == null)
                {
                    throw new NoSuchElementException("There are no more elements in the Enumeration");
                }
                LogLevel theNextElement = current;
                current = current.next();
                return theNextElement;
            }
        };
    }


    
    public static Vector asVector()
    {
        if (vector == null)
        {
            vector = new Vector(size());
            Enumeration enumeration = getEnumeration();
            while (enumeration.hasMoreElements())
            {
                LogLevel item = (LogLevel) enumeration.nextElement();
                vector.addElement(item);
            }
        }
        return vector;
    }
    
    
    /**
     * Returns an array of all <tt>LogLevel</tt>s.
     *
     * @return an array of all <tt>LogLevel</tt>s
     */
    public static LogLevel[] asArray()
    {
        if (array == null)
        {
            array = new LogLevel[upperBound];
            Enumeration enumeration = getEnumeration();
            while (enumeration.hasMoreElements())
            {
                LogLevel item = (LogLevel) enumeration.nextElement();
                array[item.ordinal] = item;
            }
        }
        return array;
    }
    /**
     * An array of the names of all <tt>LogLevel</tt>s.
     *
     * @return an array of the names of all <tt>LogLevel</tt>s.
     */
    public static String[] asArrayOfNames()
    {
        if (nameArray == null)
        {
            nameArray = new String[upperBound];
            Enumeration enumeration = getEnumeration();
            while (enumeration.hasMoreElements())
            {
                LogLevel item = (LogLevel) enumeration.nextElement();
                nameArray[item.ordinal] = item.getName();
            }
        }
        return nameArray;
    }

    /**
     * Indicates whether some other object is &quot;equal to&quot; this one. <p> The <code>equals</code> method implements an equivalence relation on non-null
     * object references: <ul> <li>It is <i>reflexive</i>: for any non-null reference value <code>x</code>, <code>x.equals(x)</code> should return
     * <code>true</code>. <li>It is <i>symmetric</i>: for any non-null reference values <code>x</code> and <code>y</code>, <code>x.equals(y)</code> should
     * return <code>true</code> if and only if <code>y.equals(x)</code> returns <code>true</code>. <li>It is <i>transitive</i>: for any non-null reference
     * values <code>x</code>, <code>y</code>, and <code>z</code>, if <code>x.equals(y)</code> returns <code>true</code> and <code>y.equals(z)</code> returns
     * <code>true</code>, then <code>x.equals(z)</code> should return <code>true</code>. <li>It is <i>consistent</i>: for any non-null reference values
     * <code>x</code> and <code>y</code>, multiple invocations of <tt>x.equals(y)</tt> consistently return <code>true</code> or consistently return
     * <code>false</code>, provided no information used in <code>equals</code> comparisons on the objects is modified. <li>For any non-null reference value
     * <code>x</code>, <code>x.equals(null)</code> should return <code>false</code>. </ul>
     *
     * @param o the reference object with which to compare.
     *
     * @return <code>true</code> if this object is the same as the obj argument; <code>false</code> otherwise.
     */
    public boolean equals(Object o)
    {
        if (this == o) { return true; }
        if (o == null || getClass() != o.getClass()) { return false; }
        LogLevel castedObject = (LogLevel) o;
        return name.equals(castedObject.name);
    }
    /**
     * An overloading of the standard {@link #equals(Object)} method that takes a <tt>String</tt> representing the name of an enumerated instance. Use of this
     * method is equivalent to:
     * <pre>
     * LogLevel.FOO.equals(LogLevel.valueOf(myLoggingLevel);
     * </pre>
     * and
     * <pre>
     * LogLevel.FOO.getName().equals(myLoggingLevel.getName());
     * </pre>
     * It is provided as a convenience method for situation where the caller is dealing with a String that maps to a <tt>LogLevel</tt>.
     *
     * @param name the name of the enumeration instance (i.e. constant) with which to compare
     *
     * @return <code>true</code> if this object is the same name as the name argument; <code>false</code> otherwise.
     */
    public boolean equals(String name)
    { return this.name.equals(name); }
    /**
     * An overloading of the standard {@link #equals(Object)} method that takes an <tt>int</tt> representing the ordinal of an enumerated instance. Use of this
     * method is equivalent to:
     * <pre>
     * LogLevel.FOO.ordinal == myLoggingLevel.ordinal;
     * </pre>
     *
     * @param ordinal the ordinal value with which to compare to
     *
     * @return<code>true</code> if this object's ordinal is the same as the ordinal argument; <code>false</code> otherwise.
     */
    public boolean equals(int ordinal)
    { return this.ordinal == ordinal; }
    public int hashCode() { return name.hashCode(); }
    /**
     * Returns the name of the enumeration instance (i.e. constant). For <tt>LogLevel.FOO</tt> this method would return &quot;FOO&quot;.
     *
     * @return the name of the enumeration instance (i.e. constant)
     */
    public String getName()
    { return this.name; }
    /**
     * The size of the enumeration set. In other words, how may enumeration instances (i.e. constants) exist.
     *
     * @return size of the enumeration set
     */
    public static int size()
    { return upperBound; }
    /**
     * Gets the first <tt>LogLevel<tt> in the enumeration set.
     *
     * @return the first <tt>LogLevel<tt> in the enumeration set
     */
    public static LogLevel first()
    { return first; }
    /**
     * Gets the last <tt>LogLevel<tt> in the enumeration set.
     *
     * @return the last <tt>LogLevel<tt> in the enumeration set.
     */
    public static LogLevel last()
    { return last; }
    /**
     * Gets a <tt>LogLevel</tt> represented name. May return <tt>null</tt> if the name received does not represent a valid enumeration instance.
     *
     * @param name the name to of the <tt>LogLevel</tt> to get
     *
     * @return the <tt>LogLevel</tt> represented by the name argument, or <tt>null</tt> if there is no <tt>LogLevel</tt> represented by the received name
     */
    public static LogLevel valueOf(String name)
    { return (LogLevel) values.get(name); }
    /**
     * Gets the previous <tt>LogLevel</tt> in the enumeration. If this instance is the first <tt>LogLevel</tt>, <tt>null</tt> is returned.
     *
     * @return the previous <tt>LogLevel</tt> or <tt>null</tt> if this is the first <tt>LogLevel</tt> in the enumeration
     */
    public LogLevel previous()
    { return this.previous; }
    /**
     * Gets the next <tt>LogLevel</tt> in the enumeration. If this instance is the last <tt>LogLevel</tt>, <tt>null</tt> is returned.
     *
     * @return the next <tt>LogLevel</tt> or <tt>null</tt> if this is the last <tt>LogLevel</tt> in the enumeration
     */
    public LogLevel next()
    { return this.next; }
    /**
     * Returns the name of the enumeration instance (i.e. constant). For <tt>LogLevel.FOO</tt> this method would return &quot;FOO&quot;.
     *
     * @return the name of the enumeration instance (i.e. constant)
     */
    public String toString()
    { return this.name; }
    /**
     * Gets the ordinal of the enumeration instance (i.e. constant). This is a convenience method since the {@link #ordinal} property itself is public.
     *
     * @return the ordinal of the enumeration instance (i.e. constant)
     */
    public int getOrdinal()
    { return ordinal; }
}
