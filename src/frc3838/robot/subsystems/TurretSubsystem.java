package frc3838.robot.subsystems;

import edu.wpi.first.wpilibj.DigitalInput;
import edu.wpi.first.wpilibj.Encoder;
import edu.wpi.first.wpilibj.Victor;
import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc3838.robot.RobotMap;
import frc3838.robot.Setup;
import frc3838.robot.commands.shooting.OperateTurretCommand;
import frc3838.robot.utils.LOG;

public class TurretSubsystem extends Subsystem
{
    //0 disable
    //1-127 CCW
    //128 - stop
    //129 - 255 CW


    public static final boolean DEBUG_TURRET = true;

    private static Victor turretPanMotorVictor;
    private static int turretPanSetting = RobotMap.Constants.TURRET_MOTOR_PWM_START;
    private static Encoder turretEncoder;
    private static DigitalInput turretCWLimit;
    private static DigitalInput turretCCWLimit;

    //static initializer block

    static
    {
        LOG.trace("TurretSubsystem initializer block executing");
        initTurretMotorPWM();
        LOG.debug("TurretSubsystem initializer block exiting");
    }

    private static void initTurretMotorPWM()
    {
        try
        {
            if (Setup.isTurretPanMotorEnabled() && turretPanMotorVictor == null)
            {
                LOG.trace("initializing turretMotorPWM");
                turretPanMotorVictor = new Victor(RobotMap.PWMChannels.TURRET_MOTOR_CHANNEL);
                turretPanMotorVictor.setRaw(turretPanSetting);

                LOG.trace("initializing turretEncoder");
                turretEncoder = new Encoder(RobotMap.DigitalIO.TURRET_ENCODER_A, RobotMap.DigitalIO.TURRET_ENCODER_B);
                turretEncoder.reset();
                turretEncoder.start();
                turretCWLimit = new DigitalInput(RobotMap.DigitalIO.TURRET_LIMIT_CW);
                turretCCWLimit = new DigitalInput(RobotMap.DigitalIO.TURRET_LIMIT_CCW);

            }
        }
        catch (Exception e)
        {
            LOG.error("An exception occurred when TurretSubsystem.initTurretMotorPWM was executed.", e);
        }
    }

    public boolean getTurretCWLimit()
    {
        return turretCWLimit.get();
    }

    public boolean getTurretCCWLimit()
    {
        return turretCCWLimit.get();
    }

    public void panTurretLeft()
    {
        if (DEBUG_TURRET)
        {
            LOG.trace("panTurretLeft called");
        }
        turretPanSetting = turretPanSetting + RobotMap.Constants.TURRET_PAN_CHANGE_AMOUNT;
        setPanValue();
    }

    public void panTurretRight()
    {
        if (DEBUG_TURRET)
        {
            LOG.trace("panTurretRight called");
        }
        turretPanSetting = turretPanSetting - RobotMap.Constants.TURRET_PAN_CHANGE_AMOUNT;
        setPanValue();
    }

    protected void setPanValue()
    {
        try
        {
            if (DEBUG_TURRET)
            {
                LOG.trace("setPanValue called");
            }

            if (Setup.isTurretPanMotorEnabled())
            {

                if (turretPanSetting > RobotMap.Constants.TURRET_PAN_MAX_VALUE)
                {
                    if (DEBUG_TURRET)
                    {
                        LOG.trace("turretPanSetting exceeds MAX VALUE, setting to MAX VALUE");
                    }
                    turretPanSetting = RobotMap.Constants.TURRET_PAN_MAX_VALUE;
                }
                else if (turretPanSetting < RobotMap.Constants.TURRET_PAN_MIN_VALUE)
                {
                    if (DEBUG_TURRET)
                    {
                        LOG.trace("turretPanSetting below MIN VALUE, setting to MIN VALUE");
                    }
                    turretPanSetting = RobotMap.Constants.TURRET_PAN_MIN_VALUE;
                }

                if (DEBUG_TURRET)
                {
                    LOG.trace("setting turret PWM raw value to " + turretPanSetting);
                }
                turretPanMotorVictor.setRaw(turretPanSetting);
            }
            else
            {
                if (DEBUG_TURRET)
                {
                    LOG.trace("Turret pan motor is disabled. Not panning. ");
                }
            }
            updateSmartDashboard();
        }
        catch (Exception e)
        {
            LOG.error("An exception occurred when CameraSubsystem.panLeft was executed.", e);
        }
    }


    public void updateSmartDashboard()
    {
        if (RobotMap.IN_DEBUG_MODE)
        {
            SmartDashboard.putBoolean("CCW Limit", getTurretCCWLimit());
            SmartDashboard.putBoolean("CW Limit", getTurretCWLimit());
            SmartDashboard.putInt("Turret motor setting", turretPanSetting);
            SmartDashboard.putInt("Turret Actual PWM Setting", turretPanMotorVictor.getRaw());
            SmartDashboard.putInt("Turret encoder", getPosition());
        }

    }

    public int getPosition()
    {
        return turretEncoder.get();
    }

    // Put methods for controlling this subsystem
    // here. Call these from Commands.
    public void initDefaultCommand()
    {
        // Set the default command for a subsystem here.
        //setDefaultCommand(new MySpecialCommand());
        setDefaultCommand(new OperateTurretCommand());
    }
}
