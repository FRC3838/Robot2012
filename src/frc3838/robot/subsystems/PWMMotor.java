package frc3838.robot.subsystems;


import edu.wpi.first.wpilibj.PWM;



public class PWMMotor
{

    /*
        From PDW documentation...
        As of revision 0.1.4 of the FPGA, the FPGA interprets the 0-255 values as follows:
          255 = full "forward"
          254 to 129 = linear scaling from "full forward" to "center"
          128 = center value
          127 to 2 = linear scaling from "center" to "full reverse"
          1 = full "reverse"
          0 = disabled (i.e. PWM output is held low)
     */

    public static final int DISABLED_VALUE = 0;
    public static final int STOP_VALUE = 128;
    public static final int RELATIVE_VALUE_MIN = 0;
    public static final int RELATIVE_VALUE_MAX = 127;

    public static final int RAW_VALUE_MIN = 0;
    public static final int RAW_VALUE_MAX = 255;


    private int currentRawValue = STOP_VALUE;
    private MotorState state = MotorState.Stopped;
    private PWMMotorSettings settings;
    private PWM motor;





    public PWMMotor(PWM motor, PWMMotorSettings settings)
    {
        this.settings = settings;
        this.motor = motor;
    }


    public PWMMotor(int motorPwmChannel, PWMMotorSettings settings)
    {
        this.settings = settings;
        this.motor = new PWM(motorPwmChannel);
    }


    public PWMMotor(int motorPwmModuleNumber, int motorPwmChannel, PWMMotorSettings settings)
    {
        this.settings = settings;
        this.motor = new PWM(motorPwmModuleNumber, motorPwmChannel);
    }


    /**
     * Increases the speed of the motor in its current direction. If the motor is stopped, the
     * target relative speed of the motor is increased
     */
    public void increaseRelativeSpeed()
    {
        if (isRunningReverse())
        {

        }
    }


    public void stopMotor()
    {
        motor.setRaw(STOP_VALUE);
        state = MotorState.Stopped;
    }

    public boolean isRunning()
    {
        return !MotorState.Stopped.equals(state);
    }


    public boolean isStopped()
    {
        return MotorState.Stopped.equals(state);
    }


    public boolean isRunningForward()
    {
        return MotorState.Forward.equals(state);
    }

    public boolean isRunningReverse()
    {
        return MotorState.Reverse.equals(state);
    }

    public MotorState getMotorDirection()
    {
        return state;
    }


    public int getCurrentRelativeSpeed()
    {
        if (isStopped())
        {
            return 0;
        }
        else if (isRunningForward())
        {
            return currentRawValue - STOP_VALUE;
        }
        else
        {
            return STOP_VALUE - currentRawValue;
        }
    }


    public int getCurrentRawValue()
    {
        return currentRawValue;
    }


    public class PWMMotorSettings
    {
        private int stepValue = 1;
        private int minForwardRelativeSpeed = RELATIVE_VALUE_MIN;
        private int maxForwardRelativeSpeed = RELATIVE_VALUE_MAX;
        private int minReverseRelativeSpeed = RELATIVE_VALUE_MIN;
        private int maxReverseRelativeSpeed = RELATIVE_VALUE_MAX;


        public PWMMotorSettings()
        {
        }


        public PWMMotorSettings(int stepValue, int minForwardRelativeSpeed, int maxForwardRelativeSpeed, int minReverseRelativeSpeed,
                                int maxReverseRelativeSpeed)
        {
            setStepValue(stepValue);
            setMinForwardRelativeSpeed(minForwardRelativeSpeed);
            setMaxForwardRelativeSpeed(maxForwardRelativeSpeed);
            setMinReverseRelativeSpeed(minReverseRelativeSpeed);
            setMaxReverseRelativeSpeed(maxReverseRelativeSpeed);
        }


        public int getMaxForwardRelativeSpeed()
        {
            return maxForwardRelativeSpeed;
        }


        public void setMaxForwardRelativeSpeed(int maxForwardRelativeSpeed)
        {
            if (maxForwardRelativeSpeed > RELATIVE_VALUE_MAX || maxForwardRelativeSpeed < minForwardRelativeSpeed)
            {
                throw new IllegalArgumentException("MaxForwardRelativeSpeed must be between the minForwardRelativeSpeed and " + RELATIVE_VALUE_MAX);
            }
            this.maxForwardRelativeSpeed = maxForwardRelativeSpeed;
        }


        public int getMaxReverseRelativeSpeed()
        {
            return maxReverseRelativeSpeed;
        }


        public void setMaxReverseRelativeSpeed(int maxReverseRelativeSpeed)
        {
            if (maxReverseRelativeSpeed > RELATIVE_VALUE_MAX || maxReverseRelativeSpeed < minReverseRelativeSpeed)
            {
                throw new IllegalArgumentException("MaxReverseRelativeSpeed must be between the minReverseRelativeSpeed and " + RELATIVE_VALUE_MAX);
            }
            this.maxReverseRelativeSpeed = maxReverseRelativeSpeed;
        }


        public int getMinForwardRelativeSpeed()
        {
            return minForwardRelativeSpeed;
        }


        public void setMinForwardRelativeSpeed(int minForwardRelativeSpeed)
        {
            if (minForwardRelativeSpeed < RELATIVE_VALUE_MIN || minForwardRelativeSpeed > maxForwardRelativeSpeed)
            {
                throw new IllegalArgumentException("MinForwardRelativeSpeed must be between " +  RELATIVE_VALUE_MIN + " and the maxForwardRelativeSpeed");
            }
            this.minForwardRelativeSpeed = minForwardRelativeSpeed;
        }


        public int getMinReverseRelativeSpeed()
        {
            return minReverseRelativeSpeed;
        }


        public void setMinReverseRelativeSpeed(int minReverseRelativeSpeed)
        {
            if (minReverseRelativeSpeed < RELATIVE_VALUE_MIN || minReverseRelativeSpeed > maxReverseRelativeSpeed)
            {
                throw new IllegalArgumentException("MinReverseRelativeSpeed must be between " + RELATIVE_VALUE_MIN + " and the maxReverseRelativeSpeed");
            }
            this.minReverseRelativeSpeed = minReverseRelativeSpeed;
        }


        public int getStepValue()
        {
            return stepValue;
        }


        public void setStepValue(int stepValue)
        {
            if (stepValue < 1 || stepValue > RELATIVE_VALUE_MAX)
            {
                throw new IllegalArgumentException("The step value must between 1 and " + RELATIVE_VALUE_MAX + " inclusively. Value received: " + stepValue);
            }
            this.stepValue = stepValue;
        }
    }

}
