package frc3838.robot.subsystems;

import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc3838.robot.Setup;
import frc3838.robot.utils.LOG;

public class ShootingCalculationsSubsystem extends Subsystem
{

    double d;    // distance to the target in ft.
    double hoopAngle; //angle that we want at the basket
    double basketHeight;  //Height of the selected basket.
    private static final double G = 32.15;  // Acceleration due to gravity.
    private static final double Y0 = 34;  // Shooter Height in inches.
    private static final double H1 = 2.333;  // low basket Height in inches.
    private static final double H2 = 5.083;  // middle basket Height in inches.
    private static final double H3 = 8.167;  // high basket  Height in inches.
    private static final double DMIN = 1.0;
    private static final double DINCR = 1.0;

    public ShootingCalculationsSubsystem(String name)
    {
        super(name);
        initCalculations();
    }

    public ShootingCalculationsSubsystem()
    {
        super();
        initCalculations();
    }

    private void initCalculations()
    {
        try
        {
            if ( Setup.isShootingCalculationsSubsystemEnabled())
            {
                hoopAngle = 31;
                d = 0.0;
                basketHeight = H3;
                SmartDashboard.putDouble("V0X", 0.0);
                SmartDashboard.putDouble("Basket Distance", d);
                SmartDashboard.putString("Basket Height: ", "Top");
            }
            else
            {
                LOG.info("ShootingCalculationsSubsystem is disabled. Will not initialize it.");
            }
        }
        catch (Exception e)
        {
            LOG.error("An exception occurred when ShootingCalculationsSubsystem.initCalculations was executed.", e);
        }
    }

    public double calcV0X(double d, double h)
    {
        double V0X = 0;
        try
        {
            V0X = 0.0;
            double TempDbl = 0.0;

            TempDbl = h - Y0;
            TempDbl = TempDbl / 12;
            TempDbl = TempDbl + d * Math.tan(Math.toRadians(hoopAngle));
            TempDbl = 1.0 / TempDbl;
            TempDbl = d * d * G * TempDbl / 2;
            V0X = Math.sqrt(TempDbl);
            SmartDashboard.putDouble("V0X", V0X);
        }
        catch (Exception e)
        {
            LOG.error("An exception occurred when ShootingCalculationsSubsystem.calcV0X was executed.", e);
        }
        return V0X;
    }

    public double incrD()
    {
        try
        {
            d = d + DINCR;
            if (d < DMIN)
            {
                d = DMIN;
            }
            SmartDashboard.putDouble("Basket Distance", d);
        }
        catch (Exception e)
        {
            LOG.error("An exception occurred when ShootingCalculationsSubsystem.incrD was executed.", e);
        }
        return d;
    }

    public double decrD()
    {
        try
        {
            d = d - DINCR;
            if (d < DMIN)
            {
                d = DMIN;
            }
            SmartDashboard.putDouble("Basket Distance", d);
        }
        catch (Exception e)
        {
            LOG.error("An exception occurred when ShootingCalculationsSubsystem.decrD was executed.", e);
        }
        return d;
    }

    public double cycleBasketHeight()
    {
        try
        {
            if (basketHeight == H1)
            {
                basketHeight = H2;
                SmartDashboard.putString("Basket Height: ", "Middle");
            }
            else
            {
                if (basketHeight == H2)
                {
                    basketHeight = H3;
                    SmartDashboard.putString("Basket Height: ", "Top");
                }
                else
                {
                    if (basketHeight == H3)
                    {
                        basketHeight = H1;
                        SmartDashboard.putString("Basket Height: ", "Bottom");
                    }
                }
            }
        }
        catch (Exception e)
        {
            LOG.error("An exception occurred when ShootingCalculationsSubsystem.cycleBasketHeight was executed.", e);
        }

        return basketHeight;
    }


    public void initDefaultCommand()
    {
        // Set the default command for a subsystem here.
        //setDefaultCommand(new MySpecialCommand());
    }
}
