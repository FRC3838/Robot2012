package frc3838.robot.subsystems;

import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.networktables.NetworkTableKeyNotDefined;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc3838.robot.commands.targeting.TargetingState;
import frc3838.robot.utils.LOG;

/**
 * Subsystem for use in interacting with the SmartDashboard VisionApp extension
 * used for target acquisition and calculations.
 */
public class TargetingSubsystem extends Subsystem
{
    public static final String KEY_STATE = "_state";
    public static final String KEY_TARGET_COUNT = "_targetCount";
    
    public static final int ERROR_VALUE = -1;

    //static initializer block
    static
    {
        LOG.trace("TargetingSubsystem initializer block executing");
        //call an init method -- such as initLight() -- for each item that needs to initialized
        LOG.debug("TargetingSubsystem initializer block exiting");
    }
    // Put methods for controlling this subsystem
    // here. Call these from Commands.


    public void changeState(TargetingState state)
    {
        SmartDashboard.putInt(KEY_STATE, state.getOrdinal());
    }

    public void changeStateToStandby()
    {
        changeState(TargetingState.STANDBY);
    }

    public void changeStateToAcquireTarget()
    {
        changeState(TargetingState.ACQUIRE_TARGET);
    }

    public boolean hasTargetBeenAcquired()
    {
        return getCurrentState().equals(TargetingState.TARGET_HAS_BEEN_ACQUIRED);
    }

    public TargetingState getCurrentState()
    {
       int ordinal;
        try
        {
            return TargetingState.valueOf(SmartDashboard.getInt(KEY_STATE));
        }
        catch (NetworkTableKeyNotDefined networkTableKeyNotDefined)
        {
            return TargetingState.ERROR;
        }
    }

    public int getTargetCount()
    {
        return SmartDashboard.getInt(KEY_TARGET_COUNT, ERROR_VALUE);
    }
    
    public void initDefaultCommand()
    {
        // Set the default command for a subsystem here.
        //setDefaultCommand(new MySpecialCommand());
    }
}
