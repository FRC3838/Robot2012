/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package frc3838.robot.subsystems;

import edu.wpi.first.wpilibj.GenericHID;
import edu.wpi.first.wpilibj.PWM;
import edu.wpi.first.wpilibj.RobotDrive;
import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc3838.robot.RobotMap;
import frc3838.robot.Setup;
import frc3838.robot.commands.drive.DriveCommand;
import frc3838.robot.utils.LOG;

public class DriveTrainSubsystem extends Subsystem
{

    private static RobotDrive robotDrive;
    // Put methods for controlling this subsystem
    // here. Call these from Commands.
    private static final int lowGearValue = 0;
    private static final int highGearValue = 255;
    private static boolean isInLowGearFlag;
    private static PWM shifterPWM;

    /**
     * Flag to determine if Robot is in an all stop state so the drive is disabled.
     */
    private boolean allStop = false;

    //static initializer block to allow for logging. Is called regardless of what constructor is called.
    static
    {
        LOG.debug("Initializing DriveTrainSubsystem");
        initRobotDrive();
        initDriveShifter();
        LOG.debug("DriveTrainSubsystem initialization completed");
    }

    private static void initRobotDrive()
    {
        try
        {
            if (Setup.isDriveTrainSubsystemEnabled())
            {
                LOG.trace("initializing robotDrive");
                robotDrive = new RobotDrive(RobotMap.PWMChannels.LEFT_WHEELS_CHANNEL, RobotMap.PWMChannels.RIGHT_WHEELS_CHANNEL);
            }
            else
            {
                LOG.info("DriveTrainSubsystem is disabled. Will not initialize it.");
            }
        }
        catch (Exception e)
        {
            LOG.error("An exception occurred when initializing the robotDrive.", e);
        }
    }

    private static void initDriveShifter()
    {
        try
        {
            if (Setup.isDriveGearShiftingEnabled())
            {
                LOG.trace("initializing shifterPWM");
                shifterPWM = new PWM(RobotMap.PWMChannels.SHIFTER_CHANNEL);
            }
            else
            {
                LOG.info("DriveGearShifting is disabled. Will not initialize it.");
            }
        }
        catch (Exception e)
        {
            LOG.error("An exception occurred when initializing the shifterPWM.", e);
        }
    }

    public DriveTrainSubsystem()
    {
        setGearFlag();
    }

    public DriveTrainSubsystem(String name)
    {
        super(name);
        setGearFlag();
    }

    /**
     * Sets the DriveTrainSubsystem to use the arcade drive.
     *
     * @param stick The joystick to use for Arcade single-stick driving. The
     * Y-axis will be selected for forwards/backwards and the X-axis will be
     * selected for rotation rate.
     * @see RobotDrive#arcadeDrive(GenericHID)
     */
    public void useArcadeDrive(GenericHID stick)
    {
        robotDrive.arcadeDrive(stick);
    }

    /**
     * Sets the DriveTrainSubsystem to use the arcade drive.
     *
     * @param stick The joystick to use for Arcade single-stick driving. The
     * Y-axis will be selected for forwards/backwards and the X-axis will be
     * selected for rotation rate.
     * @param squaredInputs If true, the sensitivity will be increased for small
     * values to increase fine control while permitting full power
     * @see RobotDrive#arcadeDrive(GenericHID, boolean)
     *
     */
    public void useArcadeDrive(GenericHID stick, boolean squaredInputs)
    {
        robotDrive.arcadeDrive(stick, squaredInputs);
    }

    private void setGearFlag()
    {
        isInLowGearFlag = shifterPWM.getRaw() == lowGearValue;
        SmartDashboard.putString("gear", getGear());
    }

    public boolean isInLowGear()
    {
        return isInLowGearFlag;
    }



    public boolean isInAllStopMode()
    {
        return allStop;
    }

    public void putInAllStopMode()
    {
        robotDrive.stopMotor();
        allStop = true;
    }

    public void exitAllStopMode()
    {
        allStop = false;
    }

    /**
     * returns gear position.
     *
     * @return "low" or "high"
     */
    public String getGear()
    {
        if (isInLowGearFlag)
        {
            return "low";
        }
        else
        {
            return "high";
        }
    }

    // Put methods for controlling this subsystem
    // here. Call these from Commands.
    public void shiftDown()
    {
        try
        {
            if (Setup.isDriveGearShiftingEnabled())
            {
                LOG.debug("Shifting to low gear. Setting PWM raw to " + lowGearValue);
                shifterPWM.setRaw(lowGearValue);
                setGearFlag();
            }
        }
        catch (Exception e)
        {
            LOG.error("An exception occurred when DriveTrainSubsystem.shiftDown was called", e);
        }
    }

    public void shiftUp()
    {
        try
        {
            if (Setup.isDriveGearShiftingEnabled())
            {
                LOG.debug("Shifting to high gear. Setting PWM raw to " + highGearValue);
                shifterPWM.setRaw(highGearValue);
                setGearFlag();
            }
        }
        catch (Exception e)
        {
            LOG.error("An exception occurred when DriveTrainSubsystem.shiftUp was executed.", e);
        }
    }

    public void toggleLimitedSpeedMode()
    {
        try
        {
            if (Setup.isDriveSpeedLimitEnabled())
            {
                LOG.trace("DriveTrainSubsystem.toggleLimitedSpeedMode called");
                RobotMap.ControlStationMap.DRIVER_JOYSTICK.toggleLimitMode();
                writeLimitedSpeedModeToDashboard();
            }
        }
        catch (Exception e)
        {
            LOG.error("An exception occurred when DriveTrainSubsystem.toggleLimitedSpeedMode was executed.", e);
        }
    }

    public void turnOnLimitSpeedMode()
    {
        try
        {
            if (Setup.isDriveSpeedLimitEnabled())
            {
                LOG.trace("DriveTrainSubsystem.turnOnLimitSpeedMode called");
                RobotMap.ControlStationMap.DRIVER_JOYSTICK.turnOnLimitMode();
                writeLimitedSpeedModeToDashboard();
            }
        }
        catch (Exception e)
        {
            LOG.error("An exception occurred when DriveTrainSubsystem.turnOnLimitSpeedMode was executed.", e);
        }
    }

    public void turnOffLimitSpeedMode()
    {
        try
        {
            if (Setup.isDriveSpeedLimitEnabled())
            {
                LOG.trace("DriveTrainSubsystem.turnOffLimitSpeedMode called");
                RobotMap.ControlStationMap.DRIVER_JOYSTICK.turnOffLimitMode();
                writeLimitedSpeedModeToDashboard();
            }
        }
        catch (Exception e)
        {
            LOG.error("An exception occurred when DriveTrainSubsystem.turnOffLimitSpeedMode was executed.", e);
        }
    }

    private void writeLimitedSpeedModeToDashboard()
    {
        boolean limited = RobotMap.ControlStationMap.DRIVER_JOYSTICK.isInLimitMode();
        SmartDashboard.putBoolean("Limited speed on", limited);
    }

    public void initDefaultCommand()
    {
        // Set the default command for a subsystem here.
        //setDefaultCommand(new MySpecialCommand());
        LOG.trace("initializing default command for to DriveCommand");
        setDefaultCommand(new DriveCommand());
        LOG.trace("initialization of default command completed");
    }
}
