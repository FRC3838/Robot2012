/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package frc3838.robot.subsystems;

import edu.wpi.first.wpilibj.Solenoid;
import edu.wpi.first.wpilibj.command.Subsystem;
import frc3838.robot.RobotMap;
import frc3838.robot.Setup;
import frc3838.robot.utils.LOG;

/**
 *
 * @author student18
 * 2/19/2012 KMS Changed to Up and Down Solenoids from a single Relay.
 */
public class BridgeArmSubsystem extends Subsystem
{
    private static Solenoid bridgeDownSolenoid;
    private static Solenoid bridgeUpSolenoid;
    
    //private static Relay bridgeArmRelay;
    private static boolean bridgeArmIsDown;

    //static initializer block
    {
        LOG.trace("BridgeArmSubsystem initializer block executing");
        initBridgeSolenoids();
        LOG.debug("BridgeArmSubsystem initializer block exiting");
    }

    private static void initBridgeSolenoids()
    {
        try
        {
            if ((Setup.isBridgeArmSubsystemEnabled()) && bridgeDownSolenoid == null)
            {
                LOG.trace("initializing BRIDGE_AIR_CYLINDER Solenoids");
                bridgeDownSolenoid = new Solenoid(RobotMap.Solenoids.BRIDGE_DOWN_AIR_CYLINDER_ADDR);
                bridgeUpSolenoid = new Solenoid(RobotMap.Solenoids.BRIDGE_UP_AIR_CYLINDER_ADDR);
                
                //bridgeArmRelay = new Relay(RobotMap.Relays.BRIDGE_AIR_CYLINDER);
                //bridgeArmRelay.setDirection(Relay.Direction.kForward);
                moveBridgeArmUpImpl();
            }
            else
            {
                LOG.info("BridgeArmSubsystem is disabled. Will not initialize it.");
            }
        }
        catch (Exception e)
        {
            LOG.error("An exception occurred when BridgeArmSubsystem.initBridgeSolenoids was executed.", e);
        }
    }


    private static void moveBridgeArmUpImpl()
    {
        try
        {
            if (Setup.isBridgeArmSubsystemEnabled())
            {
                LOG.trace("BridgeArmSubsystem.moveBridgeArmUpImpl() called");
                bridgeDownSolenoid.set(false);
                bridgeUpSolenoid.set(true);
                
                //bridgeArmRelay.set(Relay.Value.kOff);
                bridgeArmIsDown = false;
            }
        }
        catch (Exception e)
        {
            LOG.error("An exception occurred when BridgeArmSubsystem.moveBridgeArmUpImpl was executed.", e);
        }
    }

    public void moveBridgeArmUp()
    {
        moveBridgeArmUpImpl();
    }

    private static void moveBridgeArmDownImpl()
    {
        try
        {
            if (Setup.isBridgeArmSubsystemEnabled())
            {
                LOG.trace("BridgeArmSubsystem.moveBridgeArmDownImpl() called");
                bridgeUpSolenoid.set(false);
                bridgeDownSolenoid.set(true);
                
                //bridgeArmRelay.set(Relay.Value.kOn);
                bridgeArmIsDown = true;
            }
        }
        catch (Exception e)
        {
            LOG.error("An exception occurred when BridgeArmSubsystem.moveBridgeArmDownImpl was executed.", e);
        }
    }

    private static void setSolenoidsOffImpl()
    {
        try
        {
            if (Setup.isBridgeArmSubsystemEnabled())
            {
                LOG.trace("BridgeArmSubsystem.setSolenoidsOffImpl() called");
                bridgeUpSolenoid.set(false);
                bridgeDownSolenoid.set(false);
                // Bridge State should stay the same.
                //bridgeArmIsDown = true;
            }
        }
        catch (Exception e)
        {
            LOG.error("An exception occurred when BridgeArmSubsystem.moveBridgeArmDownImpl was executed.", e);
        }
    }

    public void setSolenoidsOff()
    {
        setSolenoidsOffImpl();
    }

    public void moveBridgeArmDown()
    {
        moveBridgeArmDownImpl();
    }

    /**
     * Toggles camera LED_RING ring on and off.
     * @return true if LED_RING was turned on; false is LED_RING was turned off.
     */
    public boolean toggleBridgeArm()
    {
        try
        {
            LOG.trace("BridgeArmSubsystem.toggleBridgeArm() called. Starting value of bridgeArmIsDown=" + bridgeArmIsDown);
            if (bridgeArmIsDown)
            {
                moveBridgeArmUpImpl();
            }
            else
            {
                moveBridgeArmDownImpl();
            }
            LOG.trace("BridgeArmSubsystem.toggleBridgeArm() exiting. Ending value of bridgeArmIsDown=" + bridgeArmIsDown);
        }
        catch (Exception e)
        {
            LOG.error("An exception occurred when BridgeArmSubsystem.toggleBridgeArm was executed.", e);
        }
        return bridgeArmIsDown;
    }

    public void initDefaultCommand()
    {
        // Set the default command for a subsystem here.
        //setDefaultCommand(new MySpecialCommand());
    }
}
