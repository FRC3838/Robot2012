package frc3838.robot.subsystems;

import edu.wpi.first.wpilibj.Relay;
import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc3838.robot.RobotMap;
import frc3838.robot.Setup;
import frc3838.robot.commands.shooting.RunBallLiftMotorCommand;
import frc3838.robot.utils.LOG;

public class BallLiftSubsystem extends Subsystem
{
    private static final String BALL_RAMP_DIRECTION_KEY = "Ball Lift";
    private static Relay ballLiftMotorRelay;

    //static initializer block
    static
    {
        LOG.trace("BallLiftSubsystem initializer block executing");
        initBallLiftMotor();
        LOG.debug("BallLiftSubsystem initializer block exiting");
    }

    private static void initBallLiftMotor()
    {
        try
        {
            if (Setup.isBallLiftMotorEnabled() && ballLiftMotorRelay == null)
            {
                LOG.trace("initializing ball ramp motor");
                ballLiftMotorRelay = new Relay(RobotMap.Relays.BALL_RAMP_MOTOR);
                ballLiftMotorRelay.setDirection(Relay.Direction.kBoth);
                ballLiftMotorRelay.set(Relay.Value.kOff);
            }
            else
            {
                LOG.info("Ball Lift Motor is disabled. Will not initialize it.");
            }
        }
        catch (Exception e)
        {
            LOG.error("An exception occurred when ThrowerSubsystem.initBallLiftMotor was executed.", e);
        }
    }


    public void ballLiftMotorForward()
    {
        try
        {
            if (Setup.isBallLiftMotorEnabled())
            {
                if (ballLiftMotorRelay == null)
                {
                    LOG.error("ballLiftMotorRelay is null");
                }
                else
                {
                    ballLiftMotorRelay.set(Relay.Value.kForward);
                    SmartDashboard.putString(BALL_RAMP_DIRECTION_KEY, "Forward");
                }

            }
        }
        catch (Exception e)
        {
            LOG.error("An exception occurred when ThrowerSubsystem.startBallLiftMotorForward was executed.", e);
        }
    }

    public void ballLiftMotorReverse()
    {
        try
        {
            if (Setup.isBallLiftMotorEnabled())
            {
                if (ballLiftMotorRelay == null)
                {
                    LOG.error("ballLiftMotorRelay is null");
                }
                else
                {
                    ballLiftMotorRelay.set(Relay.Value.kReverse);
                    SmartDashboard.putString(BALL_RAMP_DIRECTION_KEY, "Reverse");
                }
            }
        }
        catch (Exception e)
        {
            LOG.error("An exception occurred when ThrowerSubsystem.startBallLiftMotorReverse was executed.", e);
        }
    }

    public void ballLiftMotorOff()
    {
        try
        {
            if (ballLiftMotorRelay != null)
            {
                ballLiftMotorRelay.set(Relay.Value.kOff);
                SmartDashboard.putString(BALL_RAMP_DIRECTION_KEY, "Stopped");
            }
        }
        catch (Exception e)
        {
            LOG.error("An exception occurred when ThrowerSubsystem.ballLiftMotorOff was executed.", e);
        }
    }

    public void initDefaultCommand()
    {
        // Set the default command for a subsystem here.
        //setDefaultCommand(new MySpecialCommand());
        LOG.trace("setting BallLiftSubsystem default command to RunBallLiftMotorCommand");
        setDefaultCommand(new RunBallLiftMotorCommand());
    }
}
