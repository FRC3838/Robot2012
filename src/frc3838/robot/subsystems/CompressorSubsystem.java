package frc3838.robot.subsystems;

import edu.wpi.first.wpilibj.Compressor;
import edu.wpi.first.wpilibj.command.Subsystem;
import frc3838.robot.RobotMap;
import frc3838.robot.Setup;
import frc3838.robot.commands.RunCompressorCommand;
import frc3838.robot.utils.LOG;


public class CompressorSubsystem extends Subsystem
{
    private static Compressor compressor;

    //static initializer block
    static
    {
        LOG.trace("CompressorSubsystem initializer block executing");
        initCompressor();
        LOG.debug("CompressorSubsystem initializer block exiting");
    }

    private static void initCompressor()
    {
        try
        {
            if (Setup.isCompressorEnabled() && compressor == null)
            {
                LOG.trace("Initializing compressor");
                //Compressor(int pressureSwitchSlot, int pressureSwitchChannel, int compressorRelaySlot, int compressorRelayChannel)
                compressor = new Compressor(RobotMap.DigitalIO.COMPRESSOR_PRESSURE_SWITCH_CHANNEL,
                                            RobotMap.Relays.COMPRESSOR_RELAY_CHANNEL);
            }
            else
            {
                LOG.info("Compressor is disabled. Will not initialize it.");
            }
        }
        catch (Exception e)
        {
            LOG.error("An exception occurred when CompressorSubsystem.initCompressor was executed.", e);
        }
    }

    public void startCompressor()
    {
        try
        {
            if (Setup.isCompressorEnabled() && compressor != null)
            {
                compressor.start();
            }
        }
        catch (Exception e)
        {
            LOG.error("An exception occurred when CompressorSubsystem.startCompressor was executed.", e);
        }
    }

    public void stopCompressor()
    {
        try
        {
            if (Setup.isCompressorEnabled() && compressor != null)
            {
                compressor.stop();
            }
        }
        catch (Exception e)
        {
            LOG.error("An exception occurred when CompressorSubsystem.stopCompressor was executed.", e);
        }
    }

    /**
     * Get the pressure switch value.
     * Read the pressure switch digital input.
     *
     * @return The current state of the pressure switch.
     */
    public boolean getPressureSwitchValue()
    {
        return compressor.getPressureSwitchValue();
    }

    public void initDefaultCommand()
    {
        // Set the default command for a subsystem here.
        //setDefaultCommand(new MySpecialCommand());
        setDefaultCommand(new RunCompressorCommand());
    }
}
