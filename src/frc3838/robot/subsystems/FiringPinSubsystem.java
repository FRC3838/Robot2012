/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package frc3838.robot.subsystems;

import edu.wpi.first.wpilibj.Solenoid;
import edu.wpi.first.wpilibj.command.Subsystem;
import frc3838.robot.RobotMap;
import frc3838.robot.Setup;
import frc3838.robot.utils.LOG;


/**
 * @author student18
 */
public class FiringPinSubsystem extends Subsystem
{
    // Put methods for controlling this subsystem
    // here. Call these from Commands.
    private static Solenoid firingPinUpSolenoid; //= new Solenoid(RobotMap.Relays.FIRINGPIN_AIR_CYLINDER_SLOT, RobotMap.Relays.FIRINGPIN_UP_AIR_CYLINDER_ADDR, Direction.kForward);
    private static Solenoid firingPinDownSolenoid; //= new Solenoid(RobotMap.Relays.FIRINGPIN_AIR_CYLINDER_SLOT, RobotMap.Relays.FIRINGPIN_DOWN_AIR_CYLINDER_ADDR, Direction.kForward);

    //static initializer block
    static
    {
        LOG.trace("FiringPinSubsystem initializer block executing");
        initFiringPinSolenoids();
        LOG.debug("FiringPinSubsystem initializer block exiting");
    }

    private static void initFiringPinSolenoids()
    {
        try
        {
            if ((Setup.isFiringPinSubsystemEnabled()) && firingPinUpSolenoid == null)
            {
                LOG.trace("initializing FiringPin Solenoids");
                firingPinDownSolenoid = new Solenoid(RobotMap.Solenoids.FIRINGPIN_DOWN_AIR_CYLINDER_ADDR);
                firingPinUpSolenoid = new Solenoid(RobotMap.Solenoids.FIRINGPIN_UP_AIR_CYLINDER_ADDR);

                firingPinUpSolenoid.set(false);
                firingPinDownSolenoid.set(true);
            }
            else
            {
                LOG.info("FiringPinSubsystem is disabled. Will not initialize it.");
            }

        } catch (Exception e)
        {
            LOG.error("An exception occurred when initFiringPinRelays was executed.", e);
        }

    }

    public void fire()
    {
//        LOG.trace("FiringPinSubsystem: Fire Method.");
//        LOG.trace("FiringPinDownSolenoid False");
        firingPinDownSolenoid.set(false);
//        LOG.trace("FiringPinUpSolenoid True");
        firingPinUpSolenoid.set(true);
    }

    public void load()
    {
//        LOG.trace("FiringPinSubsystem: Load Method.");
//        LOG.trace("FiringPinUpSolenoid False");
        firingPinUpSolenoid.set(false);
//        LOG.trace("FiringPinDownSolenoid True");
        firingPinDownSolenoid.set(true);
    }

    public void off()
    {
//        LOG.trace("FiringPinSubsystem: Off Method.");
//        LOG.trace("FiringPinUpSolenoid False");
        firingPinUpSolenoid.set(false);
//        LOG.trace("FiringPinDownSolenoid False");
        firingPinDownSolenoid.set(false);
    }

    public void initDefaultCommand()
    {
        // Set the default command for a subsystem here.
        //setDefaultCommand(new MySpecialCommand());
    }
}
