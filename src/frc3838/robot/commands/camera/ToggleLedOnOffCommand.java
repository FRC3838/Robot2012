/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package frc3838.robot.commands.camera;

import frc3838.robot.commands.CommandBase;
import frc3838.robot.utils.LOG;



/**
 *
 * @author student18
 */
public class ToggleLedOnOffCommand extends CommandBase
{
    
    public ToggleLedOnOffCommand()
    {
        // Use requires() here to declare subsystem dependencies
        // eg. requires(chassis);
        requires(cameraSubsystem);
    }

    // Called just before this Command runs the first time
    protected void initialize()
    {
    }

    // Called repeatedly when this Command is scheduled to run
    protected void execute()
    {
        LOG.trace("calling cameraSubsystem.toggleLed()");
        cameraSubsystem.toggleLedRing();
    }

    // Make this return true when this Command no longer needs to run execute()
    protected boolean isFinished()
    {
        return true;
    }

    // Called once after isFinished returns true
    protected void end()
    {
    }

    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    protected void interrupted()
    {
    }
}
