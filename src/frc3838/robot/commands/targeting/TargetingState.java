package frc3838.robot.commands.targeting;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.NoSuchElementException;
import java.util.Vector;


public class TargetingState
{
    private String name;
    public final int ordinal;
    private TargetingState previous;
    private TargetingState next;
    private static TargetingState first = null;
    private static TargetingState last = null;
    private static int upperBound = -1;
    private static Hashtable values = new Hashtable();
    private static Hashtable ordinalLookup = new Hashtable();

    /** State to indicate there is an error reading the state.  */
    public static final TargetingState ERROR = new TargetingState("ERROR");
    /** State to indicate the targeting App is in Standby, waiting for a change to ACQUIRE_TARGET. */
    public static final TargetingState STANDBY = new TargetingState("STANDBY");
    /** State to indicate the Robot has asked the targeting app to acquire a target, and is waiting for such. */
    public static final TargetingState ACQUIRE_TARGET = new TargetingState("ACQUIRE_TARGET");
    /** State to indicate the Vision App has acquired a target for the Robot to shoot at. */
    public static final TargetingState TARGET_HAS_BEEN_ACQUIRED = new TargetingState("TARGET_HAS_BEEN_ACQUIRED");

    //Lazy initialized collection objects
    private static Vector vector;
    private static TargetingState[] array;
    private static String[] nameArray;



    /**
     * Constructs a new TargetingState enumeration instance (i.e. constant). This constructor is private so that outside class cannot add any enumeration instances.
     *
     * @param name the name of the enumeration instance (i.e. constant), typically in all uppercase per standard naming conventions
     */
    private TargetingState(String name)
    {
        this.name = name;
        ordinal = upperBound++;
        if (first == null) first = this;
        if (last != null)
        {
            previous = last;
            last.next = this;
        }
        last = this;
        values.put(name, this);
        ordinalLookup.put(Integer.valueOf(ordinal), this);
    }
    /**
     * Returns an {@link java.util.Enumeration} of all <tt>TargetingState</tt>s.
     *
     * @return an Enumeration of all <tt>TargetingState</tt>s
     */
    public static Enumeration getEnumeration()
    {
        return new Enumeration()
        {
            private TargetingState current = first;
            public boolean hasMoreElements()
            {
                return current != null;
            }
            public Object nextElement()
            {
                if (current == null)
                {
                    throw new NoSuchElementException("There are no more elements in the Enumeration");
                }
                TargetingState theNextElement = current;
                current = current.next();
                return theNextElement;
            }
        };
    }


    
    public static Vector asVector()
    {
        if (vector == null)
        {
            vector = new Vector(size());
            Enumeration enumeration = getEnumeration();
            while (enumeration.hasMoreElements())
            {
                TargetingState item = (TargetingState) enumeration.nextElement();
                vector.addElement(item);
            }
        }
        return vector;
    }
    
    
    /**
     * Returns an array of all <tt>TargetingState</tt>s.
     *
     * @return an array of all <tt>TargetingState</tt>s
     */
    public static TargetingState[] asArray()
    {
        if (array == null)
        {
            array = new TargetingState[upperBound];
            Enumeration enumeration = getEnumeration();
            while (enumeration.hasMoreElements())
            {
                TargetingState item = (TargetingState) enumeration.nextElement();
                array[item.ordinal] = item;
            }
        }
        return array;
    }
    /**
     * An array of the names of all <tt>TargetingState</tt>s.
     *
     * @return an array of the names of all <tt>TargetingState</tt>s.
     */
    public static String[] asArrayOfNames()
    {
        if (nameArray == null)
        {
            nameArray = new String[upperBound];
            Enumeration enumeration = getEnumeration();
            while (enumeration.hasMoreElements())
            {
                TargetingState item = (TargetingState) enumeration.nextElement();
                nameArray[item.ordinal] = item.getName();
            }
        }
        return nameArray;
    }

    /**
     * Indicates whether some other object is &quot;equal to&quot; this one. <p> The <code>equals</code> method implements an equivalence relation on non-null
     * object references: <ul> <li>It is <i>reflexive</i>: for any non-null reference value <code>x</code>, <code>x.equals(x)</code> should return
     * <code>true</code>. <li>It is <i>symmetric</i>: for any non-null reference values <code>x</code> and <code>y</code>, <code>x.equals(y)</code> should
     * return <code>true</code> if and only if <code>y.equals(x)</code> returns <code>true</code>. <li>It is <i>transitive</i>: for any non-null reference
     * values <code>x</code>, <code>y</code>, and <code>z</code>, if <code>x.equals(y)</code> returns <code>true</code> and <code>y.equals(z)</code> returns
     * <code>true</code>, then <code>x.equals(z)</code> should return <code>true</code>. <li>It is <i>consistent</i>: for any non-null reference values
     * <code>x</code> and <code>y</code>, multiple invocations of <tt>x.equals(y)</tt> consistently return <code>true</code> or consistently return
     * <code>false</code>, provided no information used in <code>equals</code> comparisons on the objects is modified. <li>For any non-null reference value
     * <code>x</code>, <code>x.equals(null)</code> should return <code>false</code>. </ul>
     *
     * @param o the reference object with which to compare.
     *
     * @return <code>true</code> if this object is the same as the obj argument; <code>false</code> otherwise.
     */
    public boolean equals(Object o)
    {
        if (this == o) { return true; }
        if (o == null || getClass() != o.getClass()) { return false; }
        TargetingState castedObject = (TargetingState) o;
        return name.equals(castedObject.name);
    }
    /**
     * An overloading of the standard {@link #equals(Object)} method that takes a <tt>String</tt> representing the name of an enumerated instance. Use of this
     * method is equivalent to:
     * <pre>
     * TargetingState.FOO.equals(TargetingState.valueOf(myLoggingLevel);
     * </pre>
     * and
     * <pre>
     * TargetingState.FOO.getName().equals(myLoggingLevel.getName());
     * </pre>
     * It is provided as a convenience method for situation where the caller is dealing with a String that maps to a <tt>TargetingState</tt>.
     *
     * @param name the name of the enumeration instance (i.e. constant) with which to compare
     *
     * @return <code>true</code> if this object is the same name as the name argument; <code>false</code> otherwise.
     */
    public boolean equals(String name)
    { return this.name.equals(name); }
    /**
     * An overloading of the standard {@link #equals(Object)} method that takes an <tt>int</tt> representing the ordinal of an enumerated instance. Use of this
     * method is equivalent to:
     * <pre>
     * TargetingState.FOO.ordinal == myLoggingLevel.ordinal;
     * </pre>
     *
     * @param ordinal the ordinal value with which to compare to
     *
     * @return<code>true</code> if this object's ordinal is the same as the ordinal argument; <code>false</code> otherwise.
     */
    public boolean equals(int ordinal)
    { return this.ordinal == ordinal; }
    public int hashCode() { return name.hashCode(); }
    /**
     * Returns the name of the enumeration instance (i.e. constant). For <tt>TargetingState.FOO</tt> this method would return &quot;FOO&quot;.
     *
     * @return the name of the enumeration instance (i.e. constant)
     */
    public String getName()
    { return this.name; }
    /**
     * The size of the enumeration set. In other words, how may enumeration instances (i.e. constants) exist.
     *
     * @return size of the enumeration set
     */
    public static int size()
    { return upperBound; }
    /**
     * Gets the first <tt>TargetingState<tt> in the enumeration set.
     *
     * @return the first <tt>TargetingState<tt> in the enumeration set
     */
    public static TargetingState first()
    { return first; }
    /**
     * Gets the last <tt>TargetingState<tt> in the enumeration set.
     *
     * @return the last <tt>TargetingState<tt> in the enumeration set.
     */
    public static TargetingState last()
    { return last; }
    /**
     * Gets a <tt>TargetingState</tt> represented name. May return <tt>null</tt> if the name received does not represent a valid enumeration instance.
     *
     * @param name the name to of the <tt>TargetingState</tt> to get
     *
     * @return the <tt>TargetingState</tt> represented by the name argument, or <tt>null</tt> if there is no <tt>TargetingState</tt> represented by the received name
     */
    public static TargetingState valueOf(String name)
    { return (TargetingState) values.get(name); }

    /**
     * Gets a <tt>TargetingState</tt> represented by an ordinal. May return <tt>null</tt> if the ordinal received does not represent a valid enumeration instance.
     *
     * @param ordinal the ordinal to of the <tt>TargetingState</tt> to get
     *
     * @return the <tt>TargetingState</tt> represented by the ordinal argument, or <tt>null</tt> if there is no <tt>TargetingState</tt> represented by the received ordinal
     */
    public static TargetingState valueOf(int ordinal)
    { return (TargetingState) values.get(Integer.valueOf(ordinal)); }

    /**
     * Gets the previous <tt>TargetingState</tt> in the enumeration. If this instance is the first <tt>TargetingState</tt>, <tt>null</tt> is returned.
     *
     * @return the previous <tt>TargetingState</tt> or <tt>null</tt> if this is the first <tt>TargetingState</tt> in the enumeration
     */
    public TargetingState previous()
    { return this.previous; }
    /**
     * Gets the next <tt>TargetingState</tt> in the enumeration. If this instance is the last <tt>TargetingState</tt>, <tt>null</tt> is returned.
     *
     * @return the next <tt>TargetingState</tt> or <tt>null</tt> if this is the last <tt>TargetingState</tt> in the enumeration
     */
    public TargetingState next()
    { return this.next; }
    /**
     * Returns the name of the enumeration instance (i.e. constant). For <tt>TargetingState.FOO</tt> this method would return &quot;FOO&quot;.
     *
     * @return the name of the enumeration instance (i.e. constant)
     */
    public String toString()
    { return this.name; }
    /**
     * Gets the ordinal of the enumeration instance (i.e. constant). This is a convenience method since the {@link #ordinal} property itself is public.
     *
     * @return the ordinal of the enumeration instance (i.e. constant)
     */
    public int getOrdinal()
    { return ordinal; }
}
