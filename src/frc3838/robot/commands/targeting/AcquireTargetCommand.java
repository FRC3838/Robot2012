package frc3838.robot.commands.targeting;

import frc3838.robot.commands.CommandBase;
import frc3838.robot.utils.LOG;

public class AcquireTargetCommand extends CommandBase
{

    public AcquireTargetCommand()
    {
        // Use requires() here to declare subsystem dependencies
        // eg. requires(chassis);
        requires(turretSubsystem);
    }

    // Called just before this Command runs the first time
    protected void initialize()
    {
        targetingSubsystem.changeStateToAcquireTarget();
    }

    // Called repeatedly when this Command is scheduled to run
    protected void execute()
    {
        //Nothing to do during execute  
    }

    // Make this return true when this Command no longer needs to run execute()
    protected boolean isFinished()
    {
        return targetingSubsystem.hasTargetBeenAcquired();
    }

    // Called once after isFinished returns true
    protected void end()
    {
        LOG.debug("AcquireTargetCommand ending. TargetCount = " + targetingSubsystem.getTargetCount());
    }

    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    protected void interrupted()
    {
    }
}
