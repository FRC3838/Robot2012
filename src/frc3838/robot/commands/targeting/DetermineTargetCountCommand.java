package frc3838.robot.commands.targeting;

import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc3838.robot.RobotMap;
import frc3838.robot.commands.CommandBase;
import frc3838.robot.utils.LOG;

public class DetermineTargetCountCommand extends CommandBase
{

    public DetermineTargetCountCommand()
    {
        // Use requires() here to declare subsystem dependencies
        // eg. requires(chassis);
        requires(targetingSubsystem);
    }

    // Called just before this Command runs the first time
    protected void initialize()
    {
    }

    // Called repeatedly when this Command is scheduled to run
    protected void execute()
    {
        int targetCount = targetingSubsystem.getTargetCount();
        LOG.debug("Target Count = " + targetCount);
        if (RobotMap.IN_DEBUG_MODE)
        {
            SmartDashboard.putInt("Target Count Fetch: ", targetCount);
        }
    }

    // Make this return true when this Command no longer needs to run execute()
    protected boolean isFinished()
    {
        return true;
    }

    // Called once after isFinished returns true
    protected void end()
    {
    }

    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    protected void interrupted()
    {
    }
}
