package frc3838.robot.commands.targeting;

import edu.wpi.first.wpilibj.command.CommandGroup;
import frc3838.robot.commands.drive.ExitAllStopModeCommand;
import frc3838.robot.commands.drive.StopDriveCommand;
import frc3838.robot.utils.LOG;

public class TargetAndShootSequence extends CommandGroup
{

    public TargetAndShootSequence()
    {
        // Add Commands here:
        // e.g. addSequential(new Command1());
        //      addSequential(new Command2());
        // these will run in order.

        // To run multiple commands at the same time,
        // use addParallel()
        // e.g. addParallel(new Command1());
        //      addSequential(new Command2());
        // Command1 and Command2 will run in parallel.

        // A command group will require all of the subsystems that each member
        // would require.
        // e.g. if Command1 requires chassis, and Command2 requires arm,
        // a CommandGroup containing them would require both the chassis and the
        // arm.


        try
        {
            addSequential(new StopDriveCommand());

            addSequential(new AcquireTargetCommand());
            addSequential(new DetermineTargetCountCommand());
            

            //TODO: Add code target and shoot;

            addSequential(new ExitAllStopModeCommand());
        }
        catch (Exception e)
        {
            LOG.error("An exception occurred when constructing the TargetAndShootSequence Command Group.", e);
        }


    }
}
