package frc3838.robot.commands;

import edu.wpi.first.wpilibj.command.Command;
import frc3838.robot.OI;
import frc3838.robot.Setup;
import frc3838.robot.subsystems.*;
import frc3838.robot.utils.LOG;

/**
 * The base for all commands. All atomic commands should subclass CommandBase.
 * CommandBase stores creates and stores each control system. To access a
 * subsystem elsewhere in your code in your code use CommandBase.exampleSubsystem
 * @author Author
 */
public abstract class CommandBase extends Command
{
    protected static OI oi;
    // Create a single static instance of all of your subsystems here; initialize them in the init method below

    protected static BridgeArmSubsystem bridgeArmSubsystem;
    protected static FiringPinSubsystem firingPinSubsystem;
    protected static CompressorSubsystem compressorSubsystem;
    protected static ShootingCalculationsSubsystem shootingCalculationsSubsystem;
    protected static CameraSubsystem cameraSubsystem;
    protected static BallLiftSubsystem ballLiftSubsystem;
    protected static DriveTrainSubsystem driveTrainSubsystem;
    protected static SenorsSubsystem senorsSubsystem;
    protected static TargetingSubsystem targetingSubsystem;
    protected static ThrowerSubsystem throwerSubsystem;
    protected static TurretSubsystem turretSubsystem;
    protected static TimingSubsystem timingSubsystem;

    public CommandBase(String name)
    {
        super(name);

    }

    public CommandBase()
    {
        super();
    }

    /**
     * Initializes all the subsystems. This is called by the robotInit() method in the
     * primary Robot class 'RocCityRobot'.
     */
    public static void init()
    {
        LOG.trace("Entering CommandBase.init()");
        LOG.debug("Initializing Subsystems (i.e. constructing each Subsystem)");
        //init our Subsystems here
        // create an init method for each subsystem; in the init method, check
        // the Setup class to see if that subsystem is enabled. You may
        // need to create the flag and the method in that class
        initTimingSubsystem();
        initCameraSubsystem();
        initSensorsSubsystem();
        initDriveTrainSubsystem();
        initThrowingSubsystem();
        initShootingCalculationsSubsystem();
        initBallLiftSubsystem();
        initTargetingSubSystem();
        initTurretSubSystem();
        initBridgeSubsystem();
        initCompressorSubsystem();
        initFiringPinSubsystem();
        
        // This MUST be here. If the OI creates Commands (which it very likely
        // will), constructing it during the construction of CommandBase (from
        // which commands extend), subsystems are not guaranteed to be
        // yet. Thus, their requires() statements may grab null pointers. Bad
        // news. Don't move it.
        LOG.trace("Calling OI constructor");
        oi = new OI();
        LOG.trace("Returned from OI Constructor call");

        // Show what command your subsystem is running on the SmartDashboard
        //SmartDashboard.putData(exampleSubsystem);


        LOG.trace("Exiting CommandBase.init()");
    }

    private static void initBridgeSubsystem()
    {
        try
        {
            if (Setup.isBridgeArmSubsystemEnabled())
            {
                LOG.trace("CommandBase.init(): constructing BridgeArmSubsystem()");
                bridgeArmSubsystem = new BridgeArmSubsystem();
            }
            else
            {
                LOG.info("CommandBase.init(): BridgeArmSubsystem is disabled. Will not initialize it.");
            }

        }
        catch (Exception e)
        {
            LOG.error("An exception occurred when initializing the BridgeArmSubsystem.", e);
        }
    }

    private static void initCompressorSubsystem()
    {
        try
        {
          if (Setup.isCompressorSubsystemEnabled())
          {
              LOG.trace("CommandBase.init(): constructing CompressorSubsystem");
              compressorSubsystem = new CompressorSubsystem();
          }
          else
          {
              LOG.info("CommandBase.init(): CompressorSubsystem is disabled. Will not initialize it.");
          }
        }
        catch (Exception e)
        {
            LOG.error("An exception occurred when initializing the CompressorSubsystem.", e);
        }
    }

    private static void initFiringPinSubsystem()
    {
        try
        {
            if (Setup.isFiringPinSubsystemEnabled() && firingPinSubsystem == null)
            {
                LOG.trace("CommandBase.init(): constructing FiringPinSubsystem");
                firingPinSubsystem = new FiringPinSubsystem();
            }
            else
            {
                LOG.info("CommandBase.init(): FiringPinSubsystem is disabled. Will not initialize it.");
            }
        }
        catch (Exception e)
        {
            LOG.error("An exception occurred when initializing the FiringPinSubsystem.", e);
        }
    }

    private static void initTimingSubsystem()
    {
        try
        {
            if (Setup.isTimingSubsystemEnabled())
            {
                LOG.trace("CommandBase.init(): constructing TimingSubsystem");
                timingSubsystem = new TimingSubsystem();
            }
            else
            {
                LOG.info("CommandBase.init(): TimingSubsystem is disabled. Will not initialize it.");
            }
        }
        catch (Exception e)
        {
            LOG.error("An exception occurred when initializing the TimingSubsystem.", e);
        }
    }

    private static void initDriveTrainSubsystem()
    {
        try
        {
            if (Setup.isDriveTrainSubsystemEnabled())
            {
                LOG.trace("CommandBase.init(): constructing DriveTrainSubsystem");
                driveTrainSubsystem = new DriveTrainSubsystem();
            }
            else
            {
                LOG.info("CommandBase.init(): DriveTrainSubsystem is disabled. Will not initialize it.");
            }
        }
        catch (Exception e)
        {
            LOG.error("An exception occurred when initializing the DriveTrainSubsystem.", e);
        }
    }

    private static void initThrowingSubsystem()
    {
        try
        {
            if (Setup.isThrowerSubsystemEnabled())
            {
                LOG.trace("CommandBase.init(): constructing ThrowerSubsystem");
                throwerSubsystem = new ThrowerSubsystem();
            }
            else
            {
                LOG.info("CommandBase.init(): ThrowerSubsystem is disabled. Will not initialize it.");
            }
        }
        catch (Exception e)
        {
            LOG.error("An exception occurred when initializing the ThrowerSubsystem.", e);
        }
    }

    private static void initShootingCalculationsSubsystem()
    {
        try
        {
            if (Setup.isShootingCalculationsSubsystemEnabled())
            {
                LOG.trace("CommandBase.init(): constructing ShootingCalculationsSubsystem");
                shootingCalculationsSubsystem = new ShootingCalculationsSubsystem();
            }
            else
            {
                LOG.info("CommandBase.init(): ShootingCalculationsSubsystem is disabled. Will not initialize it.");
            }
        }
        catch (Exception e)
        {

           LOG.error("An exception occurred when initializing the ShootingCalculationsSubsystem.", e);
        }
    }


    private static void initBallLiftSubsystem()
    {
        try
        {
            if (Setup.isBallLiftSubsystemEnabled() && ballLiftSubsystem == null)
            {
                LOG.trace("CommandBase.init(): constructing BallLiftSubsystem");
                ballLiftSubsystem = new BallLiftSubsystem();
            }
            else
            {
                LOG.info("CommandBase.init(): BallLiftSubsystem is disabled. Will not initialize it.");
            }
        }
        catch (Exception e)
        {
            LOG.error("An exception occurred when initializing the BallLiftSubsystem.", e);
        }
    }

    private static void initTurretSubSystem()
    {
        try
        {
            if (Setup.isTimingSubsystemEnabled())
            {
                LOG.trace("CommandBase.init(): constructing TurretSubsystem");
                turretSubsystem = new TurretSubsystem();
            }
            else
            {
                LOG.info("CommandBase.init(): TurretSubsystem is disabled. Will not initialize it.");
            }
        }
        catch (Exception e)
        {
            LOG.error("An exception occurred when initializing the TurretSubSystem.", e);
        }
    }

    private static void initSensorsSubsystem()
    {
        try
        {
            if (Setup.isSensorsSubsystemEnabled())
            {
                LOG.trace("CommandBase.init(): constructing SenorsSubsystem");
                senorsSubsystem = new SenorsSubsystem();
            }
            else
            {
                LOG.info("CommandBase.init(): SenorsSubsystem is disabled. Will not initialize it.");
            }
        }
        catch (Exception e)
        {
            LOG.error("An exception occurred when initializing the SenorsSubsystem.", e);
        }
    }

    public static void initTargetingSubSystem()
    {
        try
        {
            if (Setup.isTargetingSubsystemEnabled())
            {
                LOG.trace("CommandBase.init(): constructing TargetingSubsystem");
                targetingSubsystem = new TargetingSubsystem();
            }
            else
            {
                LOG.info("CommandBase.init(): TargetingSubsystem is disabled. Will not initialize it.");
            }
        }
        catch (Exception e)
        {
            LOG.error("An exception occurred when initializing the TargetingSubsystem.", e);
        }
    }


    private static void initCameraSubsystem()
    {
        try
        {
            if (Setup.isCameraSubsystemEnabled())
            {
                LOG.trace("CommandBase.init(): constructing CameraControlSubsystem");
                cameraSubsystem = new CameraSubsystem();
            }
            else
            {
                LOG.info("CommandBase.init(): CameraControlSubsystem is disabled. Will not initialize it.");
            }
        }
        catch (Exception e)
        {
            LOG.error("An exception occurred when initializing the CameraSubsystem.", e);
        }
    }
}
