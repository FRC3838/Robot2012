/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package frc3838.robot.commands;


import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;



/** @author student18 */
public class RecordSensorReadingsCommand extends CommandBase
{
    
    public RecordSensorReadingsCommand()
    {
        // Use requires() here to declare subsystem dependencies
        // eg. requires(chassis);
        requires(senorsSubsystem);
    }


    // Called just before this Command runs the first time
    protected void initialize()
    {

    }


    // Called repeatedly when this Command is scheduled to run
    protected void execute()
    {
        // Acceleration is reading 0.1032765720000004  when at a standstill
        // with a standard deviation of 0.0010290152000000492 for 25 readings
        double acceleration = senorsSubsystem.getAcceleration();
        double compassAngle = senorsSubsystem.getCompassAngle();
        double gyroAngle = senorsSubsystem.getGyroAngle();
        double distance = senorsSubsystem.getDistance();
        
        SmartDashboard.putDouble("Acceleration", acceleration);
        SmartDashboard.putDouble("Compass Angle", compassAngle);
        SmartDashboard.putDouble("Gyro Angle", gyroAngle);
        SmartDashboard.putDouble("distance", distance);
    }


    // Make this return true when this Command no longer needs to run execute()
    protected boolean isFinished()
    {
        return false;
    }


    // Called once after isFinished returns true
    protected void end()
    {
    }


    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    protected void interrupted()
    {
    }
}
