package frc3838.robot.commands.shooting;


import edu.wpi.first.wpilibj.Joystick;
import frc3838.robot.RobotMap;
import frc3838.robot.commands.CommandBase;
import frc3838.robot.subsystems.ThrowerSubsystem;
import frc3838.robot.utils.LOG;

public class ControlThrowSpeedCommand extends CommandBase
{
    private static final Joystick joystick = RobotMap.ControlStationMap.GUNNER_JOYSTICK;
    private static final int shiftBtnNum = RobotMap.ControlStationMap.GunnerJoystickButtons.THROW_MOTOR_SPEED_CHANGE_SHIFT_BTN_NUM;
    
    public ControlThrowSpeedCommand()
    {
        // Use requires() here to declare subsystem dependencies
        // eg. requires(chassis);
        requires(throwerSubsystem);
    }

    // Called just before this Command runs the first time
    protected void initialize()
    {

    }


    // Called repeatedly when this Command is scheduled to run
    protected void execute()
    {
//        if (joystick.getRawButton(shiftBtnNum))
        {
            double y = joystick.getY();
            if (ThrowerSubsystem.DEBUG_THROW_MOTOR)
            {
                LOG.debug("y value to change thrower speed read as: " + y);
            }
            boolean increase = y <= 0;
            double absValue = Math.abs(y);
            //range it
            int delta = (int) (absValue * 10);

            
            if (increase)
            {
                if (ThrowerSubsystem.DEBUG_THROW_MOTOR)
                {
                    LOG.debug("calling increaseThrowMotorSpeedSetting with a delta of " + delta);
                }
                throwerSubsystem.increaseThrowMotorSpeedSetting(delta);
            }
            else
            {
                if (ThrowerSubsystem.DEBUG_THROW_MOTOR)
                {
                    LOG.debug("calling decreaseThrowMotorSpeedSetting with a delta of " + delta);
                }
                throwerSubsystem.decreaseThrowMotorSpeedSetting(delta);
            }
        }
    }



    // Make this return true when this Command no longer needs to run execute()
    protected boolean isFinished()
    {
        return false;
    }


    // Called once after isFinished returns true
    protected void end()
    {
    }


    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    protected void interrupted()
    {
    }
}
