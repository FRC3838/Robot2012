package frc3838.robot.commands.shooting;

import frc3838.robot.commands.CommandBase;
import frc3838.robot.subsystems.ThrowerSubsystem;



public class SetThrowMotorSpeedCommand extends CommandBase
{
    private int speed = ThrowerSubsystem.RAW_VALUE_MAX;



    /**
     * Command to set the throw motor's raw speed. The speed must be between
     * the {@link frc3838.robot.subsystems.ThrowerSubsystem##RAW_VALUE_MIN_FORWARD}
     * speed and the {@link frc3838.robot.subsystems.ThrowerSubsystem#RAW_VALUE_MAX}.
     * Any value outside this range will be adjusted to within the range.
     * <b>Not that this method only sets the motor's speed. It does not start it
     * if it is not running,</b>
     * @param rawSpeed the speed to set
     */
    public SetThrowMotorSpeedCommand(int rawSpeed)
    {
        speed = rawSpeed;
        requires(throwerSubsystem);
    }

    // Called just before this Command runs the first time
    protected void initialize()
    {
    }

    // Called repeatedly when this Command is scheduled to run
    protected void execute()
    {
        throwerSubsystem.setThrowMotorSpeed(speed);
    }

    // Make this return true when this Command no longer needs to run execute()
    protected boolean isFinished()
    {
        return true;
    }

    // Called once after isFinished returns true
    protected void end()
    {
    }

    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    protected void interrupted()
    {
    }
}
