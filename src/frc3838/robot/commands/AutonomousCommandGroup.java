/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package frc3838.robot.commands;

import edu.wpi.first.wpilibj.command.CommandGroup;
import frc3838.robot.RobotMap;
import frc3838.robot.commands.shooting.FiringPinLoadCommand;
import frc3838.robot.commands.shooting.FiringPinShootCommand;
import frc3838.robot.commands.shooting.SetThrowMotorSpeedCommand;
import frc3838.robot.commands.shooting.StartBallLiftMotorCommand;
import frc3838.robot.commands.shooting.StartThrowMotorCommand;
import frc3838.robot.commands.shooting.StopBallLiftMotorCommand;
import frc3838.robot.subsystems.ThrowerSubsystem;



/**
 *
 * @author Mark
 */
public class AutonomousCommandGroup extends CommandGroup {

    public AutonomousCommandGroup()
    {
        // Add Commands here:
        // e.g. addSequential(new Command1());
        //      addSequential(new Command2());
        // these will run in order.

        // To run multiple commands at the same time,
        // use addParallel()
        // e.g. addParallel(new Command1());
        //      addSequential(new Command2());
        // Command1 and Command2 will run in parallel.

        // A command group will require all of the subsystems that each member
        // would require.
        // e.g. if Command1 requires chassis, and Command2 requires arm,
        // a CommandGroup containing them would require both the chassis and the
        // arm.



        //set speed
        addSequential(new SetThrowMotorSpeedCommand(ThrowerSubsystem.RAW_VALUE_MAX));
        //start motor
        addSequential(new StartThrowMotorCommand());
        //wait for motor to come up to speed
        addSequential(new WaitCommand(3));
        //fire!
        addFiringSequence();
        //move next ball in to chamber
        addSequential(new StartBallLiftMotorCommand());
        //give it time to load
        addSequential(new WaitCommand(5));
        //stop the ball list motor
        addSequential(new StopBallLiftMotorCommand());
        //fire # 2
        addFiringSequence();

    }


    private void addFiringSequence()
    {
        addSequential(new FiringPinShootCommand(), 1);
        //wait to ensure it has had time to move up and fire
        addSequential(new WaitCommand(500L));
        //retract the firing pin so we can load
        addSequential(new FiringPinLoadCommand(), 1);
        //give it time to retract
        addSequential(new WaitCommand(500L));
    }


    public synchronized void start()
    {
        RobotMap.inAutonomousMode = true;
        super.start();
    }


    public synchronized void cancel()
    {
        RobotMap.inAutonomousMode = false;
        super.cancel();
    }
}
