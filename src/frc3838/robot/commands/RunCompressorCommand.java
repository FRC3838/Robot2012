package frc3838.robot.commands;

import frc3838.robot.utils.LOG;

public class RunCompressorCommand extends CommandBase
{
    private static boolean DEBUG_COMPRESSOR = false;
    
    public RunCompressorCommand()
    {
        // Use requires() here to declare subsystem dependencies
        // eg. requires(chassis);
        requires(compressorSubsystem);
    }

    // Called just before this Command runs the first time
    protected void initialize()
    {
        compressorSubsystem.startCompressor();

    }

    // Called repeatedly when this Command is scheduled to run
    protected void execute()
    {
        //We do not need to do anything repeatedly. This command simply needs to initialize, and then end
        if (DEBUG_COMPRESSOR)
        {
            LOG.trace("compressor DigitalInput: " + compressorSubsystem.getPressureSwitchValue());
        }
    }

    // Make this return true when this Command no longer needs to run execute()
    protected boolean isFinished()
    {
        return false;
    }

    // Called once after isFinished returns true
    protected void end()
    {
        LOG.trace("RunCompressorCommand end() method called. Stopping compressor.");
        compressorSubsystem.stopCompressor();
    }

    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    protected void interrupted()
    {
    }
}
