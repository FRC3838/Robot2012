/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package frc3838.robot.commands.drive;

import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc3838.robot.RobotMap;
import frc3838.robot.commands.CommandBase;
import frc3838.robot.utils.LOG;


/**
 * Command to drive the robot with a Joystick.
 */
public class DriveCommand extends CommandBase
{
    private static Joystick driverJoystick = RobotMap.ControlStationMap.DRIVER_JOYSTICK;
    
    public DriveCommand()
    {
        // Use requires() here to declare subsystem dependencies
        requires(driveTrainSubsystem);
    }


    // Called just before this Command runs the first time
    protected void initialize()
    {
        LOG.debug("DriveCommand initialize() called");
    }


    // Called repeatedly when this Command is scheduled to run
    protected void execute()
    {
        if (RobotMap.IN_DEBUG_MODE)
        {
            SmartDashboard.putDouble("Driver X", driverJoystick.getX());
            SmartDashboard.putDouble("Driver Y", driverJoystick.getY());
        }
        
        if (!driveTrainSubsystem.isInAllStopMode())
        {
            driveTrainSubsystem.useArcadeDrive(driverJoystick, driverJoystick.getTrigger());
        }
    }


    // Make this return true when this Command no longer needs to run execute()
    protected boolean isFinished()
    {
        return false;
    }


    // Called once after isFinished returns true
    protected void end()
    {
    }


    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    protected void interrupted()
    {
    }
}
