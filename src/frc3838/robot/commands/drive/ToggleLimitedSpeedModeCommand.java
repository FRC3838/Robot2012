/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package frc3838.robot.commands.drive;

import frc3838.robot.commands.CommandBase;
import frc3838.robot.utils.LOG;

/**
 *
 * @author student18
 */
public class ToggleLimitedSpeedModeCommand extends CommandBase
{
    
    public ToggleLimitedSpeedModeCommand()
    {
        // Use requires() here to declare subsystem dependencies
        // eg. requires(chassis);
       requires(driveTrainSubsystem);
    }

    // Called just before this Command runs the first time
    protected void initialize()
    {
        LOG.trace("ToggleLimitedSpeedModeCommand initialize called");
    }

    // Called repeatedly when this Command is scheduled to run
    protected void execute()
    {
        driveTrainSubsystem.toggleLimitedSpeedMode();
    }
    

    // Make this return true when this Command no longer needs to run execute()
    protected boolean isFinished()
    {
        return true;
    }

    // Called once after isFinished returns true
    protected void end()
    {
    }

    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    protected void interrupted()
    {
    }
}
