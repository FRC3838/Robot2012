package frc3838.robot.components;


import edu.wpi.first.wpilibj.GenericHID;
import edu.wpi.first.wpilibj.buttons.Button;
import edu.wpi.first.wpilibj.command.Command;

public class JoystickComboButton
{
    private GenericHID joystick;
    
    private ButtonDelegate buttonADelegate;
    private ButtonDelegate buttonBDelegate;
    private ComboButtonDelegate comboButtonDelegate;
    
    
    /**
     * Create a joystick button for triggering commands
     *
     * @param joystick     The GenericHID object that has the button (e.g. Joystick, KinectStick, etc)
     * @param buttonANumber  The button number (see {@link edu.wpi.first.wpilibj.GenericHID#getRawButton(int) }
     * @param buttonBNumber  The button number that acts as the modifier
     */
    public JoystickComboButton(GenericHID joystick, int buttonANumber, int buttonBNumber)
    {
        this.joystick = joystick;
        
        buttonADelegate = new ButtonDelegate(buttonANumber, buttonBNumber);
        buttonBDelegate = new ButtonDelegate(buttonANumber, buttonANumber);
        comboButtonDelegate = new ComboButtonDelegate(buttonANumber, buttonBNumber);
    }

    public void whenPressedButtonA(Command command)
    {
        buttonADelegate.whenPressed(command);
    }

    public void whenPressedButtonB(Command command)
    {
        buttonBDelegate.whenPressed(command);
    }
    
    public void whenPressedCombined(Command command)
    {
        comboButtonDelegate.whenPressed(command);
    }

    public void whenReleasedButtonA(Command command)
    {
        buttonADelegate.whenReleased(command);
    }

    public void whenReleasedButtonB(Command command)
    {
        buttonBDelegate.whenReleased(command);
    }

    public void whenReleasedCombined(Command command)
    {
        comboButtonDelegate.whenReleased(command);
    }

    public void whileHeldButtonA(Command command)
    {
        buttonBDelegate.whileHeld(command);
    }

    public void whileHeldButtonB(Command command)
    {
        buttonADelegate.whileHeld(command);
    }

    public void whileHeldCombined(Command command)
    {
        comboButtonDelegate.whileHeld(command);
    }

    private class ComboButtonDelegate extends Button
    {
        public int buttonANumber;
        public int buttonBNumber;

        private ComboButtonDelegate(int buttonANumber, int buttonBNumber)
        {
            this.buttonANumber = buttonANumber;
            this.buttonBNumber = buttonBNumber;
        }

        public boolean get()
        {
            return joystick.getRawButton(buttonANumber) && joystick.getRawButton(buttonBNumber);
        }
    }

    private class ButtonDelegate extends Button
    {
        private int buttonNumber;
        private int alternateButtonNumber;

        private ButtonDelegate(int buttonNumber, int alternateButtonNumber)
        {
            this.buttonNumber = buttonNumber;
            this.alternateButtonNumber = alternateButtonNumber;
        }

        public boolean get()
        {
            return joystick.getRawButton(buttonNumber) && !joystick.getRawButton(alternateButtonNumber);
        }
    }
}
