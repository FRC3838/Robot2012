package frc3838.robot.components;


import edu.wpi.first.wpilibj.RobotDrive;
import edu.wpi.first.wpilibj.SpeedController;
import frc3838.robot.utils.LOG;

public class RocCityRobotDrive extends RobotDrive
{
    private boolean inLimitedSpeedMode = false;
    private static final double moveLimitValue = 0.75;
    private static final double rotateLimitValue = 0.75;

    public RocCityRobotDrive(int leftMotorChannel, int rightMotorChannel)
    {
        super(leftMotorChannel, rightMotorChannel);
    }

    public RocCityRobotDrive(int frontLeftMotor, int rearLeftMotor, int frontRightMotor, int rearRightMotor)
    {
        super(frontLeftMotor, rearLeftMotor, frontRightMotor, rearRightMotor);
    }

    public RocCityRobotDrive(SpeedController leftMotor, SpeedController rightMotor)
    {
        super(leftMotor, rightMotor);
    }

    public RocCityRobotDrive(SpeedController frontLeftMotor, SpeedController rearLeftMotor, SpeedController frontRightMotor, SpeedController rearRightMotor)
    {
        super(frontLeftMotor, rearLeftMotor, frontRightMotor, rearRightMotor);
    }

    public void arcadeDrive(double moveValue, double rotateValue, boolean squaredInputs)
    {
        try
        {
            if (inLimitedSpeedMode)
            {
                moveValue = limitValueForLimitMode(moveValue, moveLimitValue);
                rotateValue = limitValueForLimitMode(rotateValue, rotateLimitValue);
                LOG.trace("LIMITED: moveValue = " + moveValue + "  rotateValue = " + rotateValue);
            }
        } catch (Exception e)
        {
            LOG.error("An exception occurred when attempting to set the limit on the drive speed");
        }
        super.arcadeDrive(moveValue, rotateValue, squaredInputs);
    }

    private double limitValueForLimitMode(double value, double limit)
    {
        if (value > limit)
        {
            return limit;
        }
        if (value < -limit)
        {
            return -limit;
        }
        return value;
    }

    public void turnOnLimitedSpeedMode()
    {
        inLimitedSpeedMode = true;
        LOG.trace("Limited Speed Mode turned on");
    }

    public void turnOffLimitedSpeedMode()
    {
        inLimitedSpeedMode = false;
        LOG.trace("Limited Speed Mode turned off");
    }

    public boolean isInLimitedSpeedMode()
    {
        return inLimitedSpeedMode;
    }

    /**
     * Toggle limited speed mode on and off.
     * @return true if the drive is now in limited speed mode; false if the drive is no longer in limited speed mode
     */
    public boolean toggleLimitedSpeedMode()
    {
        inLimitedSpeedMode = !inLimitedSpeedMode;
        LOG.trace("Limited Speed Mode Toggled to: inLimitedSpeedMode = " + inLimitedSpeedMode);
        return inLimitedSpeedMode;
    }
}
