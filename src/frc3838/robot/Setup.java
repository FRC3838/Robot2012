package frc3838.robot;


/**
 * A class to allow for easy disabling of a subsystem or part of a subsystem. This will
 * allow for easier debugging and "emergency" shutdown of failing or errant systems
 * during the competition. A subsystem or item can be disabled easily by modifying a
 * boolean flag rather than having to find all the places necessary to comment things
 * out of the code.
 * <p />
 * For each subsystem, there should be a 'enabled' flag for the system as a whole.
 * Then there should be a flag for any parts of a subsystem that need to be
 * enabled or disabled. The boolean flags should have private access.
 * A 'is' getter should then be written for each flag. In the case of
 * subsystem parts, the getter should return the logical 'AND' of the subsystem
 * enabled flag, and that item's enabled flag.
 *
 *
 * @noinspection PointlessBooleanExpression*/
public final class Setup
{

    //CAMERA
    private static final boolean CAMERA_SUBSYSTEM_ENABLED = true;
    private static final boolean CAMERA_PAN_CONTROL_ENABLED = false;
    private static final boolean CAMERA_TILT_CONTROL_ENABLED = false;
    private static final boolean CAMERA_LED_RING_CONTROL_ENABLED = true;

    private static final boolean COMPRESSOR_SUBSYSTEM_ENABLED = true;
    private static final boolean COMPRESSOR_ENABLED = true;

    //DRIVE
    private static final boolean DRIVE_TRAIN_SUBSYSTEM_ENABLED = true;
    private static final boolean DRIVE_GEAR_SHIFTING_ENABLED = true;
    private static final boolean DRIVE_SPEED_LIMIT_ENABLED = true;

    //GENERAL
    private static final boolean BRIDGE_ARM_SUBSYSTEM_ENABLED = true;
    private static final boolean FIRINGPIN_ENABLED = true;


    //SENSORS
    private static final boolean SENSORS_SUBSYSTEM_ENABLED = true;
    private static final boolean SENSOR_ACCELEROMETER_ENABLED = false;
    private static final boolean SENSOR_COMPASS_ENABLED = true;
    private static final boolean SENSOR_GYRO_ENABLED = false;
    private static final boolean SENSOR_ULTRASOUND_ENABLED = true;


    //SHOOTING
    private static final boolean THROWER_SUBSYSTEM_ENABLED = true;
    private static final boolean THROW_MOTOR_ENABLED = true;

    private static final boolean SHOOTING_CALCULATIONS_SUBSYSTEM_ENABLED = false;

    private static final boolean TURRET_SUBSYSTEM_ENABLED = true;
    private static final boolean TURRET_PAN_MOTOR_ENABLED = true;

    private static final boolean BALL_LIFT_SUBSYSTEM_ENABLED = true;
    private static final boolean BALL_LIFT_MOTOR_ENABLED = true;
    private static final boolean BALL_LIFT_CYLINDER_ENABLED = false;
    private static final boolean BALL_LIFT_SHELF_SENSOR_ENABLED = false;
    private static final boolean BALL_LIFT_RAMP_SENSOR_ENABLED = false;

    //TARGETING
    private static final boolean TARGETING_SUBSYSTEM_ENABLED = false;

    //TIMER
    private static final boolean TIMING_SUBSYSTEM_ENABLED = false;







    private Setup() {}

    public static boolean isBallLiftSubsystemEnabled()
    {
        return BALL_LIFT_SUBSYSTEM_ENABLED;
    }

    public static boolean isBallLiftMotorEnabled()
    {
        return isBallLiftSubsystemEnabled() && BALL_LIFT_MOTOR_ENABLED;
    }

    public static boolean isBallLiftCylinderEnabled()
    {
        return isBallLiftSubsystemEnabled() && BALL_LIFT_CYLINDER_ENABLED;
    }

    public static boolean isBallLiftShelfSensorEnabled()
    {
        return isBallLiftSubsystemEnabled() && BALL_LIFT_SHELF_SENSOR_ENABLED;
    }

    public static boolean isBallLiftRampSensorEnabled()
    {
        return isBallLiftSubsystemEnabled() && BALL_LIFT_RAMP_SENSOR_ENABLED;
    }

    public static boolean isFiringPinSubsystemEnabled()
    {
        return isCompressorSubsystemEnabled() && FIRINGPIN_ENABLED;
    }

    public static boolean isCameraSubsystemEnabled()
    {
        return CAMERA_SUBSYSTEM_ENABLED;
    }


    public static boolean isCameraPanControlEnabled()
    {
        return CAMERA_SUBSYSTEM_ENABLED && CAMERA_PAN_CONTROL_ENABLED;
    }


    public static boolean isCameraTiltControlEnabled()
    {
        return CAMERA_SUBSYSTEM_ENABLED && CAMERA_TILT_CONTROL_ENABLED;
    }


    public static boolean isCameraLedRingControlEnabled()
    {
        return CAMERA_SUBSYSTEM_ENABLED && CAMERA_LED_RING_CONTROL_ENABLED;
    }

    public static boolean isShootingCalculationsSubsystemEnabled()
    {
        return SHOOTING_CALCULATIONS_SUBSYSTEM_ENABLED;
    }

    public static boolean isCompressorSubsystemEnabled()
    {
        return COMPRESSOR_SUBSYSTEM_ENABLED;
    }

    public static boolean isCompressorEnabled()
    {
        return COMPRESSOR_SUBSYSTEM_ENABLED && COMPRESSOR_ENABLED;
    }

    public static boolean isDriveTrainSubsystemEnabled()
    {
        return DRIVE_TRAIN_SUBSYSTEM_ENABLED;
    }

    public static boolean isDriveGearShiftingEnabled()
    {
        return DRIVE_TRAIN_SUBSYSTEM_ENABLED && DRIVE_GEAR_SHIFTING_ENABLED;
    }

    public static boolean isDriveSpeedLimitEnabled()
    {
        return DRIVE_TRAIN_SUBSYSTEM_ENABLED && DRIVE_SPEED_LIMIT_ENABLED;
    }

    public static boolean isBridgeArmSubsystemEnabled()
    {
        return BRIDGE_ARM_SUBSYSTEM_ENABLED;
    }

    public static boolean isSensorsSubsystemEnabled()
    {
        return SENSORS_SUBSYSTEM_ENABLED;
    }


    public static boolean isSensorAccelerometerEnabled()
    {
        return SENSORS_SUBSYSTEM_ENABLED && SENSOR_ACCELEROMETER_ENABLED;
    }


    public static boolean isSensorCompassEnabled()
    {
        return SENSORS_SUBSYSTEM_ENABLED && SENSOR_COMPASS_ENABLED;
    }


    public static boolean isSensorGyroEnabled()
    {
        return SENSORS_SUBSYSTEM_ENABLED && SENSOR_GYRO_ENABLED;
    }


    public static boolean isSensorUltrasoundEnabled()
    {
        return SENSORS_SUBSYSTEM_ENABLED && SENSOR_ULTRASOUND_ENABLED;
    }


    public static boolean isThrowerSubsystemEnabled()
    {
        return THROWER_SUBSYSTEM_ENABLED;
    }

    public static boolean isThrowMotorEnabled()
    {
        return THROWER_SUBSYSTEM_ENABLED && THROW_MOTOR_ENABLED;
    }

    public static boolean isTimingSubsystemEnabled()
    {
        return TIMING_SUBSYSTEM_ENABLED;
    }

    public static boolean isTurretSubsystemEnabled()
    {
        return TURRET_SUBSYSTEM_ENABLED;
    }

    public static boolean isTurretPanMotorEnabled()
    {
        return isTurretSubsystemEnabled() && TURRET_PAN_MOTOR_ENABLED;
    }

    public static boolean isTargetingSubsystemEnabled()
    {
        return TARGETING_SUBSYSTEM_ENABLED;
    }
}
